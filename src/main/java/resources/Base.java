package resources;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.io.FileHandler;

public class Base
	{
	WebDriver driver = null;

	public WebDriver initializeDriver(ConfigReader configReader) throws IOException
		{
			System.setProperty("webdriver." + configReader.getBrowser() + ".driver", configReader.getDriverPath());

			if (configReader.getBrowser().equals("chrome"))
				driver = new ChromeDriver();
			if (configReader.getBrowser().equals("gecko"))
				driver = new FirefoxDriver();
			if (configReader.getBrowser().equals("edge"))
				driver = new EdgeDriver();

			driver.manage().timeouts().implicitlyWait(configReader.getImplicitWait(), TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(configReader.getUrl());
			return driver;
		}

	public void getScreenshot(String result) throws IOException
		{
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileHandler.copy(src, new File("E://TestFailed_Screenshots//" + result + "screenshot.png"));
		}
	}
