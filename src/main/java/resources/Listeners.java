package resources;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Listeners implements ITestListener
	{
	private static Logger	log		= LogManager.getLogger(Listeners.class.getName());
	Base					base	= new Base();

	public void onFinish(ITestContext arg0)
		{
			log.info("All test done");
		}

	public void onStart(ITestContext arg0)
		{

		}

	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0)
		{

		}

	public void onTestFailure(ITestResult result)
		{
			log.info("TEST FAILED");
			try
				{
					base.getScreenshot(result.getName());
				}
			catch (IOException e)
				{
					e.printStackTrace();
				}
		}

	public void onTestSkipped(ITestResult arg0)
		{

		}

	public void onTestStart(ITestResult arg0)
		{

		}

	public void onTestSuccess(ITestResult arg0)
		{

		}

	}
