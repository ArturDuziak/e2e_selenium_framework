package pageObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tours
	{
	WebDriver	driver;
	Actions		actions;

	@FindBy(xpath = "/html[1]/body[1]/div[5]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/a[1]/span[1]")
	public WebElement ToursSearch;

	@FindBy(name = "date")
	public WebElement CalendarWindow;

	@FindBy(id = "adults")
	public WebElement Guests;

	@FindBy(id = "tourtype")
	public WebElement TourtypeDropDown;

	@FindBy(xpath = "/html[1]/body[1]/div[5]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[5]/button[1]")
	public WebElement SearchButton;

	@FindAll(value = @FindBy(className = "select2-result-label"))
	public List<WebElement> SearchResult;

	@FindAll(value = @FindBy(xpath = "/html[1]/body[1]/div[10]/div[1]/table[1]/tbody[1]/tr/td"))
	public List<WebElement> Calendar_Days;

	@FindAll(value = @FindBy(xpath = "/html[1]/body[1]/div[10]/div[2]/table[1]/tbody[1]/tr[1]/td[1]/span"))
	public List<WebElement> Calendar_Month;

	@FindAll(value = @FindBy(xpath = "/html[1]/body[1]/div[10]/div[3]/table[1]/tbody[1]/tr[1]/td[1]/span"))
	public List<WebElement> Calendar_Year;

	@FindAll(value = @FindBy(className = "switch"))
	public List<WebElement> Calendar_Switch_YearMonth;

	@FindBy(xpath = "//div[10]//div[1]//tr[1]//th[3]")
	public WebElement CalendarNext;

	@FindBy(xpath = "//div[10]/div[1]/table[1]/thead[1]/tr[1]/th[1]")
	public WebElement CalendarPrev;

	public Tours(WebDriver driver)
		{
			this.driver = driver;
			PageFactory.initElements(driver, this);
			this.actions = new Actions(driver);
		}

	public void Search_For_Tour(String Keyword, String ExpectedTour)
		{
			TypeIn_KeyWord(Keyword);
			Find_In_Search_Results(ExpectedTour);
		}

	public void TypeIn_KeyWord(String Keyword)
		{
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.visibilityOf(ToursSearch));
			actions.moveToElement(ToursSearch).click();
			actions.sendKeys(Keyword);
			actions.build().perform();
		}

	public void Find_In_Search_Results(String ExpectedTour)
		{
			for (int i = 0; i < SearchResult.size(); i++)
				{
					if (SearchResult.get(i).getText().equalsIgnoreCase(ExpectedTour))
						{
							SearchResult.get(i).click();
							break;
						}
				}
		}

	// Method selects number of guests (currently from 1 to 5)
	public void SetGuests(String NumberGuests)
		{
			Select select = new Select(Guests);
			select.selectByValue(NumberGuests);
		}

	// Method selects tour type (Priavte, Couples, Holidays)
	public void SetTourType(String TourType)
		{
			Select select = new Select(TourtypeDropDown);
			select.selectByVisibleText(TourType);
		}

	// Method invokikng 3 methods to set day month and year in calendar
	public void SetCalendar(String Day, String Month, String Year)
		{
			SetDay(Day);
			SetMonth(Month);
			SetYear(Year);
		}

	public void SetDay(String Day)
		{
			CalendarWindow.click();
			CalendarNext.click();

			for (int i = 0; i < Calendar_Days.size(); i++)
				{
					if (Calendar_Days.get(i).getText().equals(Day))
						{
							Calendar_Days.get(i).click();
							break;
						}
				}
		}

	public void SetMonth(String Month)
		{
			CalendarWindow.click();
			Calendar_Switch_YearMonth.get(0).click();

			for (int i = 0; i < Calendar_Month.size(); i++)
				{
					if (Calendar_Month.get(i).getText().equalsIgnoreCase(Month))
						{
							Calendar_Month.get(i).click();
							break;
						}
				}
		}

	public void SetYear(String Year)
		{
			CalendarWindow.click();
			Calendar_Switch_YearMonth.get(0).click();
			Calendar_Switch_YearMonth.get(1).click();

			for (int i = 0; i < Calendar_Year.size(); i++)
				{
					if (Calendar_Year.get(i).getText().equalsIgnoreCase(Year))
						{
							Calendar_Year.get(i).click();
							break;
						}
				}
		}
	}
