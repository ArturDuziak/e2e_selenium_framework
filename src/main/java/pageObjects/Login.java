package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login
	{
	WebDriver driver;

	@FindBy(name = "username")
	public WebElement EmailInput;

	@FindBy(name = "password")
	public WebElement PasswordInput;

	@FindBy(id = "remember-me")
	public WebElement RemberMe;

	@FindBy(css = "[type='submit']")
	public WebElement LoginButton;

	@FindBy(linkText = "Sign Up")
	public WebElement SingUp;

	@FindBy(linkText = "Forget Password")
	public WebElement ForgetPassword;

	@FindBy(xpath = "//div[@class='alert alert-danger']")
	public WebElement ErrorMsgWindow;

	final public String	LoginUrl	= "https://www.phptravels.net/demo/login";
	final public String	ErrorMsg	= "Invalid Email or Password";

	public Login(WebDriver driver)
		{
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}

	public void LoginIn(String Email, String Password)
		{
			EmailInput.sendKeys(Email);
			PasswordInput.sendKeys(Password);
			LoginButton.click();
		}

	public String GetErrorMsg()
		{
			return ErrorMsgWindow.getText();
		}

	}
