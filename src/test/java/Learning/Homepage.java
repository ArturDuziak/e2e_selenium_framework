package Learning;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import pageObjects.Hotels;
import resources.Base;
import resources.ConfigReader;

public class Homepage extends Base
	{
	WebDriver		driver;
	ConfigReader	configReader;

	@Test()
	public void basePageNavigation() throws IOException, InterruptedException
		{
			configReader = new ConfigReader();
			driver = initializeDriver(configReader);

			Hotels hotelsobj = new Hotels(driver);

			// hotelsobj.Search_For_Hotel("Kol", "Kolobrzeg, Poland");

			hotelsobj.SetDay("1");
			hotelsobj.SetMonth("Sep");
			hotelsobj.SetYear("2020");

		}
	}
