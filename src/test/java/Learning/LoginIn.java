package Learning;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import pageObjects.Login;
import resources.Base;
import resources.ConfigReader;

public class LoginIn
	{
	WebDriver				driver;
	ConfigReader			configReader;
	private static Logger	log	= LogManager.getLogger(LoginIn.class.getName());

	@BeforeMethod()
	public void Start() throws IOException
		{
			configReader = new ConfigReader();
			Base base = new Base();

			driver = base.initializeDriver(configReader);

			LandingPage landingpage = new LandingPage(driver);

			driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[2]")).click();
			landingpage.MyAccount.click();
			landingpage.Login.click();
		}

	@Test(dataProvider = "getData")
	public void Test_LoginIn(String Email, String Password, String Validation)
		{
			Login loginObj = new Login(driver);

			loginObj.LoginIn(Email, Password);

			if (Validation.equalsIgnoreCase("correct_user"))
				{
					WebDriverWait wait = new WebDriverWait(driver, 5);
					wait.until(ExpectedConditions.urlToBe("https://www.phptravels.net/demo/account/"));
					assertEquals(driver.getCurrentUrl(), "https://www.phptravels.net/demo/account/");

					log.info("Correct user successfully logged in");
				}
			else if (Validation.equalsIgnoreCase("incorrect_user"))
				{
					Assert.assertTrue(driver.getCurrentUrl().equals(loginObj.LoginUrl)
							&& loginObj.GetErrorMsg().equals(loginObj.ErrorMsg));

					log.info("Error message appeared, incorrect user was unable to login in");
				}
		}

	@AfterMethod()
	public void Closing()
		{
			driver.quit();
		}

	@DataProvider
	public Object[][] getData()
		{
			Object[][] data = new Object[5][3];
			configReader = new ConfigReader();

			// 1st row
			// Correct login and password
			data[0][0] = configReader.getLogin();
			data[0][1] = configReader.getPassword();
			data[0][2] = "correct_user";

			// 2nd row
			// No login and password
			data[1][0] = "";
			data[1][1] = "";
			data[1][2] = "incorrect_user";

			// 3rd row
			// Random login and password
			data[2][0] = "abcdef123456@gmail.com";
			data[2][1] = "123test456";
			data[2][2] = "incorrect_user";

			// 4th row
			// Correct login and incorrect password
			data[3][0] = configReader.getLogin();
			data[3][1] = "demouser123";
			data[3][2] = "incorrect_user";

			// 5th row
			// Correct password and incorrect login
			data[4][0] = "user123@phptravels.com";
			data[4][1] = configReader.getPassword();
			data[4][2] = "incorrect_user";

			return data;
		}
	}
