package Learning;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.Tours;
import resources.Base;
import resources.ConfigReader;

public class ToursTest
	{
	WebDriver		driver;
	ConfigReader	configReader;

	@BeforeMethod()
	public void Start() throws IOException
		{
			configReader = new ConfigReader();
			Base base = new Base();

			driver = base.initializeDriver(configReader);

			driver.findElement(By.cssSelector("[title='Tours']")).click();
		}

	@Test()
	public void ToursCheck()
		{
			Tours toursobj = new Tours(driver);

			toursobj.Search_For_Tour("hong", "Hong Kong, Hong Kong");
			toursobj.SetCalendar("1", "Sep", "2020");
			toursobj.SetGuests("5");
			toursobj.SetTourType("Couples");
		}
	}
